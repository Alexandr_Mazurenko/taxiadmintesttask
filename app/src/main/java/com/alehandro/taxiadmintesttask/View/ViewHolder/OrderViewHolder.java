package com.alehandro.taxiadmintesttask.View.ViewHolder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.alehandro.taxiadmintesttask.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.location)
    public TextView mLocation;
    @BindView(R.id.destination)
    public TextView mDestination;
    @BindView(R.id.timeTitle)
    public TextView mTimeTitle;
    @BindView(R.id.price)
    public TextView mPrice;

    public OrderViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
