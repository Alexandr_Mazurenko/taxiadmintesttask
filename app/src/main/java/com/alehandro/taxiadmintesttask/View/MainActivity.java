package com.alehandro.taxiadmintesttask.View;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.alehandro.taxiadmintesttask.Adapter.OrderListAdapter;
import com.alehandro.taxiadmintesttask.Constants.Constants;
import com.alehandro.taxiadmintesttask.DI.TestTaskApplication;
import com.alehandro.taxiadmintesttask.Interface.IView;
import com.alehandro.taxiadmintesttask.Presenter.MainActivityPresenter;
import com.alehandro.taxiadmintesttask.R;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.support.design.widget.TabLayout.*;

public class MainActivity extends AppCompatActivity implements IView {
    @Inject
    MainActivityPresenter presenter;
    @BindView(R.id.orderList)
    RecyclerView mOrderList;
    @BindView(R.id.tabLayout)
    TabLayout mTabLayout;
    OrderListAdapter mAdapter;
    GetOrdersBroadcastReceiver mGetOrdersBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGetOrdersBroadcastReceiver = new GetOrdersBroadcastReceiver();
        registerBroadcastreceiver();
        TestTaskApplication.getViewComponent().inject(this);
        presenter.setView(this);
        setContentView(R.layout.activity_main);
        initUI();
    }

    @Override
    public void initUI() {
        ButterKnife.bind(this);
        mOrderList.setHasFixedSize(true);
        mOrderList.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new OrderListAdapter();
        mOrderList.setAdapter(mAdapter);
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.online).setText("Online"));
        mTabLayout.addTab(mTabLayout.newTab().setIcon(R.drawable.preorder).setText("PreOrder"));
        mTabLayout.addOnTabSelectedListener(new OnTabSelectedListener() {
            @Override
            public void onTabSelected(Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        presenter.setOrderMode(Constants.ACTUAL_ORDERS);
                        break;
                    case 1:
                        presenter.setOrderMode(Constants.PRE_ORDERS);
                        break;
                }
            }

            @Override
            public void onTabUnselected(Tab tab) {
            }

            @Override
            public void onTabReselected(Tab tab) {
            }
        });
    }

    @Override
    public void updateUI() {
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void registerBroadcastreceiver() {
        registerReceiver(mGetOrdersBroadcastReceiver, new IntentFilter(Constants
                .TIME_MESSAGE_INTENT_FILTER));
    }

    @Override
    public void unregisterBroadcastReceiver() {
        unregisterReceiver(mGetOrdersBroadcastReceiver);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        unregisterBroadcastReceiver();
        super.onDestroy();
    }

    private class GetOrdersBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            presenter.getOrdersFromWeb();
        }
    }
}
