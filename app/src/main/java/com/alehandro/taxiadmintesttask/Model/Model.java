package com.alehandro.taxiadmintesttask.Model;

import com.alehandro.taxiadmintesttask.Constants.Constants;
import com.alehandro.taxiadmintesttask.POJO.Order;

import java.util.ArrayList;
import java.util.List;

public class Model {
    private List<Order> mActualOrderList;
    private List<Order> mPreliminaryOrderList;
    private double mLongitude;
    private double mLatitude;
    private String mOrderMode;

    public Model() {
        mActualOrderList = new ArrayList<>();
        mPreliminaryOrderList = new ArrayList<>();
        mOrderMode = Constants.ACTUAL_ORDERS;
        setStubPreliminaryOrders();
    }


    public List<Order> getActualOrderList() {
        return mActualOrderList;
    }

    public List<Order> getCurrentOrderList() {
        if (mOrderMode.equals(Constants.ACTUAL_ORDERS)) {
            return mActualOrderList;
        } else
            return mPreliminaryOrderList;
    }

    public List<Order> getPreliminaryOrderList() {
        return mPreliminaryOrderList;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public void setLongitude(double longitude) {
        this.mLongitude = longitude;
    }

    public double getLatitude() {
        return mLatitude;
    }

    public void setLatitude(double latitude) {
        this.mLatitude = latitude;
    }

    public String getRequestUrl() {
        return new StringBuilder()
                .append(Constants.SERVER_URL)
                .append(Constants.ID_CAR_TAG + Constants.REQUEST_LOGIN)
                .append(Constants.PASS_TAG + Constants.REQUEST_PASS)
                .append(Constants.GET_ORDER_TAG + Constants.REQUEST_ORDER_DEFAULT)
                .append(Constants.X_TAG + String.valueOf(mLongitude))
                .append(Constants.Y_TAG + String.valueOf(mLatitude))
                .toString();
    }

    public String getOrderMode() {
        return mOrderMode;
    }

    public void setOrderMode(String mOrderMode) {
        this.mOrderMode = mOrderMode;
    }

    public void setStubPreliminaryOrders() {
        for (int i = 0; i < 25; i++) {
            Order order = new Order();
            order.setDestination("Пушкинская 25/" + i);
            order.setLocation("Садовая 25");
            order.setPrice("100");
            order.setDate("25.03.17");
            mPreliminaryOrderList.add(order);
        }
    }

}
