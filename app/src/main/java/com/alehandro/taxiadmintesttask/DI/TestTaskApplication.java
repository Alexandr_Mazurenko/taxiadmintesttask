package com.alehandro.taxiadmintesttask.DI;

import android.app.Application;
import android.content.Intent;

import com.alehandro.taxiadmintesttask.Service.OrderService;

public class TestTaskApplication extends Application {
    private static AppComponent sAppComponent;
    private static ViewComponent sViewComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sAppComponent = buildAppComponent();
        sViewComponent = buildViewComponent();
        startService(new Intent(this, OrderService.class));
    }

    private ViewComponent buildViewComponent() {
        return DaggerViewComponent.builder()
                .viewModule(new ViewModule())
                .build();
    }

    private AppComponent buildAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

    public static ViewComponent getViewComponent() {
        return sViewComponent;
    }

    @Override
    public void onTerminate() {
        stopService(new Intent(this, OrderService.class));
        super.onTerminate();
    }
}
