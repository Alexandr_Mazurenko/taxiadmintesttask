package com.alehandro.taxiadmintesttask.DI;

import com.alehandro.taxiadmintesttask.Presenter.MainActivityPresenter;
import com.alehandro.taxiadmintesttask.Presenter.OrderListAdapterPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ViewModule {

    @Provides
    @Singleton
    MainActivityPresenter getMainActivityPresenter() {
        return new MainActivityPresenter();
    }

    @Provides
    @Singleton
    OrderListAdapterPresenter getOrderListAdapterPresenter() {
        return new OrderListAdapterPresenter();
    }

}
