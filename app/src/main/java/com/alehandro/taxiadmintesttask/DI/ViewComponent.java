package com.alehandro.taxiadmintesttask.DI;

import com.alehandro.taxiadmintesttask.Adapter.OrderListAdapter;
import com.alehandro.taxiadmintesttask.View.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ViewModule.class)
public interface ViewComponent {
    void inject(MainActivity mainActivity);

    void inject(OrderListAdapter orderListAdapter);
}
