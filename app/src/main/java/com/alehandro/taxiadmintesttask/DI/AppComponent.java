package com.alehandro.taxiadmintesttask.DI;

import com.alehandro.taxiadmintesttask.Presenter.MainActivityPresenter;
import com.alehandro.taxiadmintesttask.Presenter.OrderListAdapterPresenter;
import com.alehandro.taxiadmintesttask.Service.OrderService;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(MainActivityPresenter mainActivityPresenter);

    void inject(OrderListAdapterPresenter orderListAdapterPresenter);

    void inject(OrderService orderService);
}
