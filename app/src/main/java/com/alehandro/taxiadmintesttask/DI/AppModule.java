package com.alehandro.taxiadmintesttask.DI;

import android.app.Application;
import android.content.Context;
import android.location.LocationManager;

import com.alehandro.taxiadmintesttask.Model.Model;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    Application mApplication;

    public AppModule(Application application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Model getModel() {
        return new Model();
    }

    @Provides
    @Singleton
    LocationManager getLocationManager() {
        return (LocationManager) mApplication.getSystemService(Context.LOCATION_SERVICE);
    }


}
