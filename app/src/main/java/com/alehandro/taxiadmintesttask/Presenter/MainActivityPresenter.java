package com.alehandro.taxiadmintesttask.Presenter;

import android.os.AsyncTask;
import android.util.Log;

import com.alehandro.taxiadmintesttask.Constants.Constants;
import com.alehandro.taxiadmintesttask.DI.TestTaskApplication;
import com.alehandro.taxiadmintesttask.Interface.IView;
import com.alehandro.taxiadmintesttask.Interface.IViewPresenter;
import com.alehandro.taxiadmintesttask.Model.Model;
import com.alehandro.taxiadmintesttask.POJO.Order;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.inject.Inject;

public class MainActivityPresenter implements IViewPresenter {
    @Inject
    Model mModel;
    private AsyncTask<Void, Void, Void> mOrderTask;
    private Document mHtmlOrderRequest;
    private Elements mElements;
    private List<Order> bufferList;
    private WeakReference<IView> mIViewWeakReference;
    private IView mIView;


    public MainActivityPresenter() {
        TestTaskApplication.getAppComponent().inject(this);
    }

    @Override
    public void setView(IView iView) {
        mIViewWeakReference = new WeakReference<>(iView);
        mIView = mIViewWeakReference.get();
    }

    public void setOrderMode(String mode) {
        mModel.setOrderMode(mode);
        mIView.updateUI();
    }

    @Override
    public void getOrdersFromWeb() {
        mOrderTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                bufferList = new ArrayList<>();
                try {
                    List<String> items;
                    items = new ArrayList<>();
                    mHtmlOrderRequest = Jsoup.connect(mModel.getRequestUrl()).get();
                    mHtmlOrderRequest.normalise();
                    //Log.i(Constants.LOG_TAG,"html_request:"+mHtmlOrderRequest.html());
                    mElements = mHtmlOrderRequest.select("order");
                    //Log.i(Constants.LOG_TAG, "order_request:" + mElements.html());
                    //get array of items
                    String itemList[] = mElements.html().split("<br>");
                    for (String string : itemList) {
                        //create a separate string from every item
                        if (!string.isEmpty()) {
                            items.add(string);
                        }
                        //Log.i(Constants.LOG_TAG, "order_items:" + string);

                    }
                    for (String item : items) {
                        //Log.i(Constants.LOG_TAG,"item:"+item);
                        StringTokenizer tokenizer = new StringTokenizer(item, "|");
                        Order order = new Order();
                        //Log.i(Constants.LOG_TAG,"tokens:"+tokenizer.countTokens());
                        int count = 1;
                        while (tokenizer.hasMoreTokens()) {
                            String tokenizerResult = tokenizer.nextToken();
                            if (count == 2) {
                                order.setLocation(tokenizerResult);
                            }
                            if (count == 3) {
                                order.setDestination(tokenizerResult);
                            }
                            if (count == 9) {
                                order.setPrice(tokenizerResult);
                            }
                            if (count == 12) {
                                order.setDate(tokenizerResult);
                            }
                            count++;
                        }
                        bufferList.add(order);
                    }

                } catch (IOException e) {
                    Log.i(Constants.LOG_TAG, "exception:" + e.getLocalizedMessage());
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                for (Order order : bufferList) {
                    Log.i(Constants.LOG_TAG, "\nOrderList: Location-" + order.getLocation()
                            + " Destination-" + order.getDestination() + " Price-" + order.getPrice()
                            + " Date-" + order.getDate());
                }
                mModel.getActualOrderList().clear();
                mModel.getActualOrderList().addAll(bufferList);
                mIView.showToastMessage("data is refreshed");
                mIView.updateUI();
            }
        };
        mOrderTask.execute();
    }


}
