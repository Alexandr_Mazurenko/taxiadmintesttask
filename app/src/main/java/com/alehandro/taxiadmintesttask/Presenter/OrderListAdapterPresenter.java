package com.alehandro.taxiadmintesttask.Presenter;

import com.alehandro.taxiadmintesttask.DI.TestTaskApplication;
import com.alehandro.taxiadmintesttask.Interface.IAdapterPresenter;
import com.alehandro.taxiadmintesttask.Model.Model;
import com.alehandro.taxiadmintesttask.POJO.Order;

import java.util.List;

import javax.inject.Inject;

public class OrderListAdapterPresenter implements IAdapterPresenter {
    @Inject
    Model mModel;

    public OrderListAdapterPresenter() {
        TestTaskApplication.getAppComponent().inject(this);
    }

    @Override
    public int getItemCount() {
        return mModel.getActualOrderList().size();
    }

    @Override
    public List<Order> getCurrentOrderList() {
        return mModel.getCurrentOrderList();
    }
}
