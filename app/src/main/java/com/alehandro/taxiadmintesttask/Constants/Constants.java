package com.alehandro.taxiadmintesttask.Constants;

public class Constants {
    public final static String TIME_MESSAGE_INTENT_FILTER = "timeMessageFilter";
    public final static String LOG_TAG = "TaxiAdminTestTask";
    public final static String ACTUAL_ORDERS = "actualOrders";
    public final static String PRE_ORDERS = "preOrders";

    public final static String SERVER_URL = "http://89.184.67.115/taxi/index.php?";
    public final static String ID_CAR_TAG = "id_car=";
    public final static String PASS_TAG = "&pass=";
    public final static String GET_ORDER_TAG = "&get_order=";
    public final static String X_TAG = "&x=";
    public final static String Y_TAG = "&y=";

    public final static String REQUEST_LOGIN = "31";
    public final static String REQUEST_PASS = "123456";
    public final static String REQUEST_ORDER_DEFAULT = "1";
}
