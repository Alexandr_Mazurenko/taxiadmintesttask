package com.alehandro.taxiadmintesttask.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.alehandro.taxiadmintesttask.DI.TestTaskApplication;
import com.alehandro.taxiadmintesttask.Presenter.OrderListAdapterPresenter;
import com.alehandro.taxiadmintesttask.R;
import com.alehandro.taxiadmintesttask.View.ViewHolder.OrderViewHolder;

import javax.inject.Inject;

public class OrderListAdapter extends RecyclerView.Adapter<OrderViewHolder> {
    @Inject
    OrderListAdapterPresenter presenter;

    public OrderListAdapter() {
        super();
        TestTaskApplication.getViewComponent().inject(this);
    }

    @Override
    public OrderViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new OrderViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(OrderViewHolder holder, int position) {
        holder.mLocation.setText(presenter.getCurrentOrderList().get(position).getLocation());
        holder.mDestination.setText(presenter.getCurrentOrderList().get(position).getDestination());
        holder.mTimeTitle.setText(presenter.getCurrentOrderList().get(position).getDate());
        holder.mPrice.setText(presenter.getCurrentOrderList().get(position).getPrice());
    }

    @Override
    public int getItemCount() {
        return presenter.getItemCount();
    }
}
