package com.alehandro.taxiadmintesttask.Service;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.alehandro.taxiadmintesttask.Constants.Constants;
import com.alehandro.taxiadmintesttask.DI.TestTaskApplication;
import com.alehandro.taxiadmintesttask.Model.Model;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

public class OrderService extends Service {
    @Inject
    Model mModel;
    @Inject
    LocationManager mLocationManager;

    private LocationListener mLocationListener;
    private String mBestprovider;
    private Location mLastKnownLocation;
    private Timer mTimer = null;


    public OrderService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        TestTaskApplication.getAppComponent().inject(this);
        mTimer = new Timer();
        mTimer.scheduleAtFixedRate(new OrderTimeTask(), TimeUnit.SECONDS.toMillis(3),
                TimeUnit.SECONDS.toMillis(3));
        //create location listener
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                //Log.i(Constants.LOG_TAG,"onLocationChanged\n");
                //Log.i(Constants.LOG_TAG,"Longitude:"+location.getLongitude());
                mModel.setLongitude(location.getLongitude());
                //Log.i(Constants.LOG_TAG,"Latitude:"+location.getLatitude());
                mModel.setLatitude(location.getLatitude());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                Log.i(Constants.LOG_TAG, "\nonStatusChanged\n");
                Log.i(Constants.LOG_TAG, "status:" + status);
            }

            @Override
            public void onProviderEnabled(String provider) {
                Log.i(Constants.LOG_TAG, "\nEnabled:" + provider);
            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.i(Constants.LOG_TAG, "\nDisabled:" + provider);
            }
        };
        //get the best provider
        //mBestprovider = mLocationManager.getBestProvider(new Criteria(), false);
        mBestprovider = LocationManager.NETWORK_PROVIDER;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        //get default last know value
        mLastKnownLocation = mLocationManager.getLastKnownLocation(mBestprovider);
        if (mLastKnownLocation != null) {
            Log.i(Constants.LOG_TAG, "\ngetLastKnownLocation\n");
            Log.i(Constants.LOG_TAG, "Longitude:" + mLastKnownLocation.getLongitude());
            mModel.setLongitude(mLastKnownLocation.getLongitude());
            Log.i(Constants.LOG_TAG, "Latitude:" + mLastKnownLocation.getLatitude());
            mModel.setLatitude(mLastKnownLocation.getLatitude());
        }
        //register request
        mLocationManager.requestLocationUpdates(mBestprovider, 0, 0, mLocationListener);
    }

    @Override
    public void onDestroy() {
        mLocationManager.removeUpdates(mLocationListener);
        super.onDestroy();
    }

    private class OrderTimeTask extends TimerTask {

        @Override
        public void run() {
            sendBroadcast(new Intent(Constants.TIME_MESSAGE_INTENT_FILTER));
        }
    }
}
