package com.alehandro.taxiadmintesttask.Interface;

import com.alehandro.taxiadmintesttask.POJO.Order;

import java.util.List;

public interface IAdapterPresenter {
    int getItemCount();

    List<Order> getCurrentOrderList();
}
