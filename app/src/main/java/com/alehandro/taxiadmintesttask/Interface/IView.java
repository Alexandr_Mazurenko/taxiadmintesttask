package com.alehandro.taxiadmintesttask.Interface;

public interface IView {
    void initUI();

    void updateUI();

    void registerBroadcastreceiver();

    void unregisterBroadcastReceiver();

    void showToastMessage(String message);
}
